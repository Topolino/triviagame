# Name: Joey Carberry
# Date: October 6, 2015
# Project: Project7 Trivia Game

import trivia

def questions1():
    nC = 0

    # Question One
    Trivia = trivia.Trivia('Which U.S. Founding Father discovered the Gulf Stream?','George Washington', 'George Lopez', 'Ben Franklin', 'Donald')
    print('Question:',Trivia.question())
    print('1.)', Trivia.ans1())
    print('2.)', Trivia.ans2())
    print('3.)', Trivia.ans3())
    print('4.)', Trivia.ans4())
    ans = int(input('Answer: '))
    if ans == 3:
        print('Correct!')
        nC += 1
    else:
        print('Incorrect.')
        print('The correct answer is: ',Trivia.ans3())
    print('Number correct: ', nC)

    # Question Two
    Trivia = trivia.Trivia('In what state is it illegal to tie a giraffe to a light post?','Vermont', 'Flordia', 'Nevada', 'California')
    print('Question:',Trivia.question())
    print('1.)', Trivia.ans1())
    print('2.)', Trivia.ans2())
    print('3.)', Trivia.ans3())
    print('4.)', Trivia.ans4())
    ans = int(input('Answer: '))
    if ans == 1:
        print('Correct!')
        nC += 1
    else:
        print('Incorrect.')
        print('The correct answer is: ',Trivia.ans1())
    print('Number correct: ', nC)

    # Question Three
    Trivia = trivia.Trivia('Which kills more people than sharks?','Vending Machines', 'Coconuts', 'Hot Dogs', 'All the above')
    print('Question:',Trivia.question())
    print('1.)', Trivia.ans1())
    print('2.)', Trivia.ans2())
    print('3.)', Trivia.ans3())
    print('4.)', Trivia.ans4())
    ans = int(input('Answer: '))
    if ans == 4:
        print('Correct!')
        nC += 1
    else:
        print('Incorrect.')
        print('The correct answer is: ',Trivia.ans4())
    print('Number correct: ', nC)

    # Question Four
    Trivia = trivia.Trivia('Who was involved in inventing the internet?','Al Gore', 'Vint Cerf', 'Don Schiefstein', 'Simon Eggman')
    print('Question:',Trivia.question())
    print('1.)', Trivia.ans1())
    print('2.)', Trivia.ans2())
    print('3.)', Trivia.ans3())
    print('4.)', Trivia.ans4())
    ans = int(input('Answer: '))
    if ans == 2:
        print('Correct!')
        nC += 1
    else:
        print('Incorrect.')
        print('The correct answer is: ',Trivia.ans2())
    print('Number correct: ', nC)

    # Question Five
    Trivia = trivia.Trivia('What is the tallest mountain in Alaska?','Mount McKinley', 'Mount Rainer', 'Mount Sebastian', 'Denali')
    print('Question:',Trivia.question())
    print('1.)', Trivia.ans1())
    print('2.)', Trivia.ans2())
    print('3.)', Trivia.ans3())
    print('4.)', Trivia.ans4())
    ans = int(input('Answer: '))
    if ans == 4:
        print('Correct!')
        nC += 1
    else:
        print('Incorrect.')
        print('The correct answer is: ',Trivia.ans4())
    print('Number correct: ', nC)

def questions2():
    nC2 = 0
    print('Let the second player play.')
    # Questions 6
    Trivia = trivia.Trivia('What was the Nintendo company originally?','Trading Card Company', 'Coconut Company', 'Arcade Company', 'Software Company')
    print('Question:',Trivia.question())
    print('1.)', Trivia.ans1())
    print('2.)', Trivia.ans2())
    print('3.)', Trivia.ans3())
    print('4.)', Trivia.ans4())
    ans = int(input('Answer: '))
    if ans == 1:
        print('Correct!')
        nC2 += 1
    else:
        print('Incorrect.')
        print('The correct answer is: ',Trivia.ans1())
    print('Number correct: ', nC2)

    #Questions 7
    Trivia = trivia.Trivia('Which State renamed Columbus Day to Indigenous People Day?','North Dakota', 'Oregon', 'Alaska', 'Flordia')
    print('Question:',Trivia.question())
    print('1.)', Trivia.ans1())
    print('2.)', Trivia.ans2())
    print('3.)', Trivia.ans3())
    print('4.)', Trivia.ans4())
    ans = int(input('Answer: '))
    if ans == 3:
        print('Correct!')
        nC2 += 1
    else:
        print('Incorrect.')
        print('The correct answer is: ',Trivia.ans3())
    print('Number correct: ', nC2)

    # Question 8
    Trivia = trivia.Trivia('Who was the first Emperor of China?','Qui', 'Chia', 'Qouguin', 'Chi')
    print('Question:',Trivia.question())
    print('1.)', Trivia.ans1())
    print('2.)', Trivia.ans2())
    print('3.)', Trivia.ans3())
    print('4.)', Trivia.ans4())
    ans = int(input('Answer: '))
    if ans == 1:
        print('Correct!')
        nC2 += 1
    else:
        print('Incorrect.')
        print('The correct answer is: ',Trivia.ans1())
    print('Number correct: ', nC2)

    # Questions 9
    Trivia = trivia.Trivia('What city contained the Great Library?','Rome', 'Alexandria', 'Babylon', 'Constantinople')
    print('Question:',Trivia.question())
    print('1.)', Trivia.ans1())
    print('2.)', Trivia.ans2())
    print('3.)', Trivia.ans3())
    print('4.)', Trivia.ans4())
    ans = int(input('Answer: '))
    if ans == 2:
        print('Correct!')
        nC2 += 1
    else:
        print('Incorrect.')
        print('The correct answer is: ',Trivia.ans2())
    print('Number correct: ', nC2)

    # Question 9
    Trivia = trivia.Trivia('What ingredient was often used in the 1920-1930s in cosmetics?','Cocaine', 'Lead', 'Arsenic', 'Asbestos')
    print('Question:',Trivia.question())
    print('1.)', Trivia.ans1())
    print('2.)', Trivia.ans2())
    print('3.)', Trivia.ans3())
    print('4.)', Trivia.ans4())
    ans = int(input('Answer: '))
    if ans == 3:
        print('Correct!')
        nC2 += 1
    else:
        print('Incorrect.')
        print('The correct answer is: ',Trivia.ans3())
    print('Number correct: ', nC2)

    # Question 10
    Trivia = trivia.Trivia('What was the original color of carrots?','Blue', 'Green', 'Yellow', 'Purple')
    print('Question:',Trivia.question())
    print('1.)', Trivia.ans1())
    print('2.)', Trivia.ans2())
    print('3.)', Trivia.ans3())
    print('4.)', Trivia.ans4())
    ans = int(input('Answer: '))
    if ans == 4:
        print('Correct!')
        nC2 += 1
    else:
        print('Incorrect.')
        print('The correct answer is: ',Trivia.ans4())
    print('Number correct: ', nC2)

    # End Game
questions1()
questions2()
