# Name: Joey Carberry
# Date: October 6, 2015
# Project: Project7 class

class Trivia:

    def __init__(self, question, ans1, ans2, ans3, ans4):
        self.__question = question
        self.__ans1 = ans1
        self.__ans2 = ans2
        self.__ans3 = ans3
        self.__ans4 = ans4

    def question(self):
        return self.__question

    def ans1(self):
        return self.__ans1

    def ans2(self):
        return self.__ans2

    def ans3(self):
        return self.__ans3

    def ans4(self):
        return self.__ans4



